app.controller('PlanoDeVooController', function($rootScope, $timeout, AppService, PlanoDeVooService, AeroportoService, TipoAeronaveService, 
     $scope, toaster, $mdDialog, $q){

    $rootScope.pageTitle = "Planos de Vôo";

    $scope.listaPlanoDeVoo = [];
    $scope.listaAeroporto = [];
    $scope.listaTipoAeronave = [];

    $scope.selected = [];
    $scope.frmPlanoDeVoo = null;

    var planoDeVooSelecionado;
           
    TipoAeronaveService.getAll().then(function(response){
        $scope.listaTipoAeronave = response.data.map(function(item){
            return {
                id: item.Id,
                nome: item.Nome
            }
        })
    })

    AeroportoService.getAll().then(function(response){
        $scope.listaAeroporto = response.data.map(function(item){
            return {
                id: item.Id,
                sigla: item.Sigla
            }
        })
    })

    loadTable();
      
    $scope.Remover = function(ev){
        var conteudoConfirm = {
            title:'Você tem certeza que deseja remover esse Plano de Vôo ?',
            textContent:'Uma vez excluído, não será possível reverter essa operação.'
        }
        
        var promise = AppService.showConfirm(ev, conteudoConfirm);

        promise.then(function(){
            PlanoDeVooService.delete($scope.selected[0].id).then(function(){
                AppService.showSimpleToast('Plano de Vôo removido.');
                $scope.selected = [];
                loadTable();
            })
        }, function(){
            
        })
    }

    $scope.Detalhar = function(ev){
        
        $scope.tituloDialog = 'Atualiza Plano de Vôo';
        
        PlanoDeVooService.get(planoDeVooSelecionado.id).then(function(response){

            $scope.frmPlanoDeVoo = {
                Id:response.data.Id,
                numeroVoo:response.data.numeroVoo,
                dataHoraVoo: new Date(response.data.dataHoraVoo),
                origemId: response.data.origemId,
                destinoId: response.data.destinoId,
                tipoAeronaveId: response.data.tipoAeronaveId,
                matriculaAeronave: response.data.matriculaAeronave
            }

            AppService.showAdvanced(ev, $scope, 'app/partials/dialog-form-plano-de-voo.html');

        })

        

    }

    $scope.Novo = function(ev){
        
        $scope.tituloDialog = 'Novo Plano de Vôo';
        $scope.frmPlanoDeVoo = null;
        AppService.showAdvanced(ev, $scope, 'app/partials/dialog-form-plano-de-voo.html');
    }

    $scope.SalvarPlanoDeVoo = function(){
        
        if($scope.frmPlanoDeVoo.Id === undefined || $scope.frmPlanoDeVoo.Id === null  || $scope.frmPlanoDeVoo.Id === ''){
            
            PlanoDeVooService.post($scope.frmPlanoDeVoo).then(function(){
                $scope.frmPlanoDeVoo = null;
                AppService.showSimpleToast('Plano de Vôo adicionado');
                loadTable();
            })

        }
        else{

            PlanoDeVooService.put($scope.frmPlanoDeVoo).then(function(){
                $scope.frmPlanoDeVoo = null;
                AppService.showSimpleToast('Plano de Vôo atualizado');
                loadTable();
            })

        }

        AppService.CloseDialog();
        

        $scope.selected = [];
        
    }

    $scope.options = {
        rowSelection: true,
        multiSelect: false,
        autoSelect: true,
        decapitate: false,
        largeEditDialog: false,
        boundaryLinks: false,
        limitSelect: true,
        pageSelect: true
    };
    
    $scope.deselect = function (item) {
        planoDeVooSelecionado = null;
    };

    $scope.log = function (item) {
        planoDeVooSelecionado = item;
    };

    $scope.query = {
        order: 'numeroVoo',
        page: 1
    };

    $scope.removeFilter = function () {
        $scope.filter.show = false;
        $scope.filter.search = '';
        
        if($scope.filter.form.$dirty) {
          $scope.filter.form.$setPristine();
        }
    };

    $scope.CloseDialog = function() {
        $mdDialog.cancel();
    };

    function loadTable(){

        PlanoDeVooService.getAll().then(function(response){
            $scope.listaPlanoDeVoo = response.data.map(function(item){
                return {
                    id: item.Id,
                    numeroVoo:item.numeroVoo,
                    dataHoraVoo: item.dataHoraVoo,
                    origem: item.AeroportoOrigem,
                    destino: item.AeroportoDestino,
                    matriculaAeronave: item.matriculaAeronave
                }
            })

            $scope.$evalAsync();
            
        })

    }

})