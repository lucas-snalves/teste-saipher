app.service('PlanoDeVooService', function($rootScope, $http){

    var service = this;

    

    service.get = function(id){
        return $http.get($rootScope.urlAPI + '/PlanoDeVoo/' +id);
    }

    service.getAll = function(){
        return $http.get($rootScope.urlAPI + '/PlanoDeVoo');
    }

    service.delete = function(id){
        console.log(id);
        return $http.delete($rootScope.urlAPI + '/PlanoDeVoo/' + id);

    }

    service.post = function(PlanoDeVoo){

        PlanoDeVoo.dataHoraVoo = new Date(PlanoDeVoo.dataHoraVoo + 'UTC');
        return $http.post($rootScope.urlAPI +  '/PlanoDeVoo', JSON.stringify(PlanoDeVoo));

    }

    service.put = function(PlanoDeVoo){

        PlanoDeVoo.dataHoraVoo = new Date(PlanoDeVoo.dataHoraVoo + 'UTC');
        return $http.put($rootScope.urlAPI + '/PlanoDeVoo/' + PlanoDeVoo.Id, PlanoDeVoo);

    }
    

})