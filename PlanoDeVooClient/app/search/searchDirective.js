app.directive('search', function(){
    
    return {
        templateUrl: 'app/search/search.html',
        restrict: 'E'
    }
})