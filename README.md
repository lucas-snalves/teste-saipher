# README #

Esse README é o documento com os passos necessários para rodar a aplicação.

### Sobre o projeto ###

Projeto CRUD de teste para o processo seletivo da Saipher.
O projeto foi desenvolvido em ASP.NET, utilizando o recurso Web API, com o conceito RESTful.
No lado Client, foi desenvolvido em angularJS, juntamente com o angular material.

### Instruções para rodar a aplicação ###

A API foi desenvolvida na IDE Visual Studio 17.

Database configuration:
SQL Server 2008
Tabelas:
Aeroporto (utilizado para Origem e Destino do plano de Vôo)
TipoAeronave (utilizado para Tipo da Aeronava do Plano de Vôo)
Plano de Vôo (recebe FKs de Aeroporto e TipoAeronave)

Client(PlanoDeVooClient):
O projeto client está na pasta plano-de-voo.
É necessário que seja rodada dentro de um server (IIS, xampp, wamp, etc), pois está fazendo o uso de requisições http.
A url de acesso a API está no arquivo PlanoDeVooClient/app/js/app.js, na linha 29:
$rootScope.urlAPI = 'http://localhost:53859/Api';

O projeto backend está configurado para rodar no caminho acima, mas caso seja instalado no IIS, basta alterar a url de conexão, conforme mencionado acima.

Backend(PlanoDeVooWebApi):
A API foi desenvolvida utilizando entityframework (code-first). O banco de dados será criado ao startar a aplicação e adicionará registros nas tabelas:
Aeroporto e TipoAeronave

A única configuração necessária a se fazer é no arquivo WebConfig, inserindo as informações de acesso ao banco de dados, conforme abaixo:
<add name="localhost" connectionString="data source=ICSJC-152\SQLEXPRESS;initial catalog=plano-de-voo;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework" providerName="System.Data.SqlClient" />
connectionString=url de conexão
catalog=nome do banco de dados que será criado.

Para rodar a aplicação (IIS Express), basta abrir a solution, configurar o WebConfig e startar.


