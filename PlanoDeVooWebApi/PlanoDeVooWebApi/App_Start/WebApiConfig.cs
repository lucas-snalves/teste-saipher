﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using PlanoDeVooWebApi.Models;

namespace PlanoDeVooWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute(origins: "*", headers: "*", methods: "*");
            config.EnableCors(cors);
            // Serviços e configuração da API da Web

            // Rotas da API da Web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            DateTime data = DateTime.Now; 

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            using (Contexto _contexto = new Contexto())
            {
                if (!_contexto.Database.Exists())
                {
                    _contexto.Database.CreateIfNotExists();
                    
                    List<Aeroporto> aeroportos = new List<Aeroporto>();
                    aeroportos.Add(new Aeroporto("SBGR"));
                    aeroportos.Add(new Aeroporto("SBSP"));
                    aeroportos.Add(new Aeroporto("SBCF"));

                    _contexto.Aeroporto.AddRange(aeroportos);

                    List<TipoAeronave> tiposAeronaves = new List<TipoAeronave>();
                    tiposAeronaves.Add(new TipoAeronave("A320"));
                    tiposAeronaves.Add(new TipoAeronave("A330"));
                    tiposAeronaves.Add(new TipoAeronave("B777"));

                    _contexto.TipoAeronave.AddRange(tiposAeronaves);

                    _contexto.SaveChanges();
                }

                
            }
        }
    }
}
