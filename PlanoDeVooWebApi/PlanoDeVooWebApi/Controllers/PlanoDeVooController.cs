﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PlanoDeVooWebApi.Models;

namespace PlanoDeVooWebApi.Controllers
{
    public class PlanoDeVooController : ApiController
    {
        private Contexto db = new Contexto();

        // GET: api/PlanoDeVoo
        public IQueryable<PlanoDeVoo> GetPlanoDeVoo()
        {
            return db.PlanoDeVoo;
        }

        // GET: api/PlanoDeVoo/5
        [ResponseType(typeof(PlanoDeVoo))]
        public IHttpActionResult GetPlanoDeVoo(int id)
        {
            PlanoDeVoo planoDeVoo = db.PlanoDeVoo.Find(id);
            if (planoDeVoo == null)
            {
                return NotFound();
            }

            return Ok(planoDeVoo);
        }

        // PUT: api/PlanoDeVoo/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPlanoDeVoo(int id, PlanoDeVoo planoDeVoo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != planoDeVoo.Id)
            {
                return BadRequest();
            }

            db.Entry(planoDeVoo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlanoDeVooExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PlanoDeVoo
        [ResponseType(typeof(PlanoDeVoo))]
        public IHttpActionResult PostPlanoDeVoo(PlanoDeVoo planoDeVoo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PlanoDeVoo.Add(planoDeVoo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = planoDeVoo.Id }, planoDeVoo);
        }

        // DELETE: api/PlanoDeVoo/5
        [ResponseType(typeof(PlanoDeVoo))]
        public IHttpActionResult DeletePlanoDeVoo(int id)
        {
            PlanoDeVoo planoDeVoo = db.PlanoDeVoo.Find(id);
            if (planoDeVoo == null)
            {
                return NotFound();
            }

            db.PlanoDeVoo.Remove(planoDeVoo);
            db.SaveChanges();

            return Ok(planoDeVoo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PlanoDeVooExists(int id)
        {
            return db.PlanoDeVoo.Count(e => e.Id == id) > 0;
        }
    }
}