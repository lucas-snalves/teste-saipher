﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PlanoDeVooWebApi.Models;

namespace PlanoDeVooWebApi.Controllers
{
    public class TipoAeronaveController : ApiController
    {
        private Contexto db = new Contexto();

        // GET: api/TipoAeronave
        public IQueryable<TipoAeronave> GetTipoAeronave()
        {
            return db.TipoAeronave;
        }

        // GET: api/TipoAeronave/5
        [ResponseType(typeof(TipoAeronave))]
        public IHttpActionResult GetTipoAeronave(int id)
        {
            TipoAeronave tipoAeronave = db.TipoAeronave.Find(id);
            if (tipoAeronave == null)
            {
                return NotFound();
            }

            return Ok(tipoAeronave);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoAeronaveExists(int id)
        {
            return db.TipoAeronave.Count(e => e.Id == id) > 0;
        }
    }
}