﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PlanoDeVooWebApi.Models;

namespace PlanoDeVooWebApi.Controllers
{
    public class AeroportoController : ApiController
    {
        private Contexto db = new Contexto();

        // GET: api/Aeroporto
        public IQueryable<Aeroporto> GetAeroporto()
        {
            return db.Aeroporto;
        }

        // GET: api/Aeroporto/5
        [ResponseType(typeof(Aeroporto))]
        public IHttpActionResult GetAeroporto(int id)
        {
            Aeroporto aeroporto = db.Aeroporto.Find(id);
            if (aeroporto == null)
            {
                return NotFound();
            }

            return Ok(aeroporto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AeroportoExists(int id)
        {
            return db.Aeroporto.Count(e => e.Id == id) > 0;
        }
    }
}