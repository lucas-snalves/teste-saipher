﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlanoDeVooWebApi.Models
{
    [Table("TipoAeronave")]
    public class TipoAeronave
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Nome { get; set; }

        public TipoAeronave(string nome)
        {
            this.Nome = nome;
        }
        public TipoAeronave() { }

    }
}