﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlanoDeVooWebApi.Models
{
    [Table("PlanoDeVoo")]
    public class PlanoDeVoo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "O número do vôo é obrigatório.")]
        public string numeroVoo { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime dataHoraVoo { get; set; }

        public int origemId { get; set; }
        public int destinoId { get; set; }

        public int tipoAeronaveId { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "A matrícula da Aeronave é obrigatória.")]
        public string matriculaAeronave { get; set; }

        [ForeignKey("origemId")]
        public virtual Aeroporto AeroportoOrigem { get; set; }

        [ForeignKey("destinoId")]
        public virtual Aeroporto AeroportoDestino { get; set; }

        [ForeignKey("tipoAeronaveId")]
        public virtual TipoAeronave TipoAeronave { get; set; }
    }
}