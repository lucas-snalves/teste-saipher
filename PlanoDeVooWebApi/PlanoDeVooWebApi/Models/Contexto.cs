﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PlanoDeVooWebApi.Models
{
    public class Contexto : DbContext
    {
        public Contexto() : base("localhost") { }

        public DbSet<PlanoDeVoo> PlanoDeVoo { get; set; }
        public DbSet<Aeroporto> Aeroporto { get; set; }
        public DbSet<TipoAeronave> TipoAeronave { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            
        }
    }
}