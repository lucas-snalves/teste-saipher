﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PlanoDeVooWebApi.Models
{
    [Table("Aeroporto")]
    public class Aeroporto
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(10)]
        public string Sigla { get; set; }

        public Aeroporto(string sigla)
        {
            this.Sigla = sigla;
        }

        public Aeroporto() { }

    }
}